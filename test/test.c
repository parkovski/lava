#include "parse.h"
#include "rax/rax.h"
#include "symtab.h"

#include <string.h>
#include <ctype.h>
#include <assert.h>

const char source[] =
  "# Some comment\n"
  "mov foo, bar\n"
  "add bar, baz\n"
  "nop\n"
  "ldstr 'hello \\'world\\''\n"
  "ldstr \"goodbye \\\"world\\\"\"\n"
  "ldint 123\n"
  "ldflt 1.23\n"
  "# end comment"
  ;

static int test_lex() {
  lava_parser_ctx parser;
  lava_parser_init(&parser, source, sizeof source);

  do {
    printf("%d:%d-%d:%d (%s)",
           parser.tok.span.start.line, parser.tok.span.start.col,
           parser.tok.span.end.line, parser.tok.span.end.col,
           lava_tkname(parser.tok.kind));
    if (parser.tok.kind > lava_tk_space) {
      int len = (int)(parser.tok.span.end.ofs - parser.tok.span.start.ofs);
      const char *s = parser.buf + parser.tok.span.start.ofs;
      printf(": %.*s\n", len, s);
    } else {
      printf("\n");
    }
    lava_scan(&parser);
  } while (parser.tok.kind != lava_tk_eof);

  lava_parser_fini(&parser);
  return 0;
}

static int test_parse() {
  lava_parser_ctx parser;
  lava_parser_init(&parser, source, sizeof source);
  lava_instr_node ins;

  while (true) {
    ins.op.kind = lava_tk_none;
    ins.arg[0].kind = lava_tk_none;
    ins.arg[1].kind = lava_tk_none;
    ins.arg[2].kind = lava_tk_none;
    lava_parse_instr(&parser, &ins);
    if (ins.op.kind == lava_tk_none) {
      break;
    }
    int end_line = ins.op.span.end.line;
    for (unsigned i = 1; i < 3; ++i) {
      if (ins.arg[i].kind != lava_tk_none) {
        end_line = ins.arg[i].span.end.line;
      }
    }
    size_t len = ins.op.span.end.ofs - ins.op.span.start.ofs;
    printf("%03d-%03d %.*s", ins.op.span.start.line, end_line,
           (int)len, parser.buf + ins.op.span.start.ofs);

    for (unsigned i = 0; i < 3; ++i) {
      if (ins.arg[i].kind == lava_tk_none) {
        break;
      }
      printf("%s[%s]%.*s",
             i > 0 ? ", " : " ",
             lava_tkname(ins.arg[i].kind),
             (int)(ins.arg[i].span.end.ofs - ins.arg[i].span.start.ofs),
             parser.buf + ins.arg[i].span.start.ofs);
    }
    putchar('\n');
  }

  lava_parser_fini(&parser);
  return 0;
}

static int test_symbol() {
  lava_symtab st;
  lava_symbol *s;
  uint32_t id_root, id_name, id_num, id_byte, id_i32, id_var;
  size_t addr;

  lava_symtab_init(&st);
  lava_symtab_add_core(&st);

  id_root = st.size;
  s = lava_symbol_new(&st, 0, "test_root", 9, 0);
  assert(s->count == 0);
  assert(s->id == id_root);

  id_name = st.size;
  s = lava_symbol_new(&st, id_root, "name", 4, 1, lava_id_mem_info32);
  assert(s->count == 1);
  assert(s->id == id_name);
  assert(s->attrs[0].id == lava_id_mem_info32);
  assert(s->attrs[0].offset == sizeof(lava_symbol) + sizeof(lava_attr_info));
  ((lava_mem_info32 *)lava_symbol_attr(s, 0))->align = LAVA_MEM_INFO_ALIGN_WORD;
  ((lava_mem_info32 *)lava_symbol_attr(s, 0))->size = sizeof(size_t);

  id_num = st.size;
  s = lava_symbol_new(&st, id_root, "num", 3, 1, id_name);
  assert(s->count == 1);
  assert(s->attrs[0].id == id_name);
  assert(s->attrs[0].offset == sizeof(lava_symbol) + sizeof(lava_attr_info));
  *((const char **)lava_symbol_attr(s, 0)) = "num";

  id_byte = st.size;
  s = lava_symbol_new(&st, id_root, "byte", 4, 3, lava_id_mem_info32, id_name, id_num);
  assert(s->count == 3);
  assert(s->attrs[0].id == lava_id_mem_info32);
  assert(s->attrs[0].offset == sizeof(lava_symbol) + 3 * sizeof(lava_attr_info));
  assert(s->attrs[1].id == id_name);
  addr = (size_t)lava_symbol_attr(s, 1);
  assert((addr & (sizeof(size_t) - 1)) == 0);
  assert(s->attrs[2].id == id_num);
  assert(s->attrs[2].offset == 0);
  ((lava_mem_info32 *)lava_symbol_attr(s, 0))->align = LAVA_MEM_INFO_ALIGN_1B;
  ((lava_mem_info32 *)lava_symbol_attr(s, 0))->size = 1;
  *((const char **)lava_symbol_attr(s, 1)) = "byte";

  // swap order of attributes, it shouldn't matter.
  id_i32 = st.size;
  s = lava_symbol_new(&st, id_root, "i32", 3, 3, id_name, id_num, lava_id_mem_info32);
  assert(s->count == 3);
  assert(s->attrs[0].id == id_name);
  assert(s->attrs[0].offset == sizeof(lava_symbol) + 3 * sizeof(lava_attr_info));
  assert(s->attrs[1].id == id_num);
  assert(s->attrs[1].offset == 0);
  assert(s->attrs[2].id == lava_id_mem_info32);
  assert(s->attrs[2].offset == sizeof(lava_symbol) + 3 * sizeof(lava_attr_info) + sizeof(char *));
  *((const char **)lava_symbol_attr(s, 0)) = "i32";
  ((lava_mem_info32 *)lava_symbol_attr(s, 2))->align = LAVA_MEM_INFO_ALIGN_4B;
  ((lava_mem_info32 *)lava_symbol_attr(s, 2))->size = 4;

  id_var = st.size;
  s = lava_symbol_new(&st, id_root, "var", 3, 2, id_byte, id_name);
  assert(s->count == 2);
  assert(s->attrs[0].id == id_byte);
  assert(s->attrs[0].offset == sizeof(lava_symbol) + 2 * sizeof(lava_attr_info));
  assert(s->attrs[1].id == id_name);
  assert(s->attrs[1].offset > sizeof(lava_symbol) + 2 * sizeof(lava_attr_info) + 1);
  size_t pname = (size_t)lava_symbol_attr(s, 1);
  assert((pname & (sizeof(size_t) - 1)) == 0);

  lava_symtab_fini(&st);
  return 0;
}

static int test_name() {
  lava_symtab st;
  lava_symbol *s;
  uint32_t id_root;
  lava_symtab_init(&st);
  lava_symtab_add_core(&st);

  assert(lava_symtab_find_name(&st, -1, "empty", 5) == st.symbols[lava_id_empty]);
  assert(lava_symtab_find_name(&st, 0, "mem_info32", 10) == st.symbols[lava_id_mem_info32]);

  s = lava_symbol_new(&st, 0, "test_root", 9, 0);
  assert(s);
  id_root = s->id;
  assert(lava_symtab_find_name(&st, 0, "test_root", 9) == s);
  assert(lava_symtab_find_name(&st, 1, "test_root", 9) == NULL);

  s = lava_symbol_new(&st, id_root, "new_name", 8, 0);
  assert(s);
  assert(lava_symtab_find_name(&st, 0, "new_name", 8) == NULL);
  assert(lava_symtab_find_name(&st, id_root, "new_name", 8) == s);

  s = lava_symbol_new(&st, id_root, "empty", 5, 0);
  assert(s);
  assert(lava_symtab_find_name(&st, id_root, "empty", 5) == s);
  assert(lava_symtab_find_name(&st, 0, "empty", 5) == NULL);

  lava_symtab_fini(&st);
  return 0;
}

int main(int argc, char **argv) {
  if (argc != 2) {
    printf("usage: test [<test name> | all | list]\n");
    return 1;
  }

  int r = 0;
  rax *map = raxNew();
#define ADD_TEST(Name) \
  raxInsert(map, (unsigned char *)#Name, sizeof #Name - 1, &test_##Name, NULL)

  ADD_TEST(lex);
  ADD_TEST(parse);
  ADD_TEST(symbol);
  ADD_TEST(name);

#undef ADD_TEST

  if (!strcmp(argv[1], "all")) {
    raxIterator iter;
    raxStart(&iter, map);
    raxSeek(&iter, ">=", NULL, 0);
    while (raxNext(&iter)) {
      r += ((int (*)())iter.data)();
    }
    raxStop(&iter);
  } else if (!strcmp(argv[1], "list")) {
    printf("test list:\n");
    raxIterator iter;
    raxStart(&iter, map);
    raxSeek(&iter, ">=", NULL, 0);
    while (raxNext(&iter)) {
      printf(" - %.*s\n", (int)iter.key_len, iter.key);
    }
    raxStop(&iter);
  } else {
    void *fn = raxFind(map, (unsigned char*)(argv[1]), strlen(argv[1]));
    if (fn == raxNotFound) {
      printf("no test %s\n", argv[1]);
      r = 1;
    } else {
      r = ((int (*)())fn)();
    }
  }

  raxFree(map);
  return r;
}
