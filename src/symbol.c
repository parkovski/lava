#include "symbol.h"

#include <string.h>
#include <stdalign.h>
#include <assert.h>

uint32_t lava_symbol_find_attr_index(lava_symbol *s, uint32_t id) {
  for (uint32_t i = 0; i < s->count; ++i) {
    if (s->attrs[i].id == id) {
      return i;
    }
  }
  return lava_id_undefined;
}

void *lava_symbol_find_attr(lava_symbol *s, uint32_t id) {
  uint32_t index = lava_symbol_find_attr_index(s, id);
  if (index == lava_id_undefined) {
    return NULL;
  }
  return lava_symbol_attr(s, index);
}
