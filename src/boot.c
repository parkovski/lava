#include "parse.h"
#include "exec.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

static size_t read_all(char *filename, char **buf) {
  FILE *file;
  unsigned long size;
  unsigned long amt;
  unsigned long ofs;

  *buf = NULL;

  file = fopen(filename, "r");
  if (!file) {
    fprintf(stderr, "File open failed for '%s'.\n", filename);
    errno = ENOENT;
    return 0;
  }

  fseek(file, 0, SEEK_END);
  size = (unsigned long)ftell(file);
  if (size == (unsigned long)-1L) {
    fprintf(stderr, "Unknown file size.\n");
    errno = EIO;
    goto finish;
  }
  fseek(file, 0, SEEK_SET);

  *buf = malloc(size + 1);
  if (!*buf) {
    fprintf(stderr, "Out of memory.\n");
    errno = ENOMEM;
    goto finish;
  }

  ofs = 0;
  while ((amt = fread(*buf + ofs, 1, size - ofs, file)) != 0) {
    ofs += amt;
  }
  if (ofs != size) {
    fprintf(stderr, "Read error (%ld/%ld).\n", ofs, size);
    errno = EIO;
    goto fail_io;
  }

  goto finish;

fail_io:
  free(*buf);
  *buf = NULL;

finish:
  fclose(file);
  return size;
}

static void run_parser(const char *buf, size_t size) {
  lava_parser_ctx parser;
  lava_parser_init(&parser, buf, size);
  while (true) {
    lava_instr_node ins;
    memset(&ins, 0, sizeof(lava_instr_node));
    lava_parse_instr(&parser, &ins);
    if (ins.op.kind == lava_tk_none) {
      break;
    }
    printf("%.*s; ",
           (int)(ins.op.span.end.ofs - ins.op.span.start.ofs),
           parser.buf + ins.op.span.start.ofs);
  }
  putchar('\n');
}

int main(int argc, char **argv) {
  char *buf;
  size_t size;
  if (argc != 2) {
    fprintf(stderr, "Missing filename.\n");
    errno = EINVAL;
    goto fail_inval;
  }

  errno = 0;
  size = read_all(argv[1], &buf);
  if (errno) {
    goto finish;
  }

  run_parser(buf, size);

finish:
  free(buf);

fail_inval:
  return errno;
}
