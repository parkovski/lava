#include "symtab.h"

#include <stdlib.h>
#include <stdarg.h>
#include <alloca.h>
#include <string.h>

// Static functions {{{

static void append_attr(uint32_t *size, lava_attr_info *attr, lava_symbol *s) {
  lava_mem_info32 *m = lava_symbol_find_attr(s, lava_id_mem_info32);
  if (m) {
    uint32_t align = 1U << m->align;
    uint32_t aligned_offset = (*size + align - 1) & -align;
    *size = aligned_offset + m->size;
    *attr = (lava_attr_info) {
      .id = s->id,
      .offset = aligned_offset,
    };
  } else {
    *attr = (lava_attr_info) {
      .id = s->id,
      .offset = 0,
    };
  }
}

static lava_symbol *init_symbol(lava_symtab *st, uint32_t size,
                                uint32_t parent, const char *name,
                                uint32_t len, uint32_t count,
                                lava_attr_info *attrs) {
  uint32_t id = lava_symtab_reserve(st);
  if (id == lava_id_undefined) {
    return NULL;
  }

  lava_symbol *s = malloc(size);
  if (!s) {
    return NULL;
  }

  s->id = id;
  s->parent = parent;
  s->name = name;
  s->namelen = len;
  s->count = count;
  memcpy(s->attrs, attrs, count * sizeof(lava_attr_info));

  if (!lava_symtab_insert_name(st, s)) {
    free(s);
    return NULL;
  }

  st->symbols[id] = s;

  return s;
}

// }}}

bool lava_symtab_init(lava_symtab *st) {
  st->size = 0;
  if (!(st->symbols = malloc(sizeof(lava_symbol *) * LAVA_SYMVEC_MIN_GROW))) {
    goto fail;
  }
  st->capacity = LAVA_SYMVEC_MIN_GROW;

  if (!(st->names = raxNew())) {
    goto fail_free_vec;
  }

  return true;

fail_free_vec:
  free(st->symbols);

fail:
  st->capacity = 0;
  st->names = NULL;

  return false;
}

void lava_symtab_fini(lava_symtab *st) {
  // TODO free each symbol?
  free(st->symbols);
  raxFree(st->names);
}

uint32_t lava_symtab_reserve(lava_symtab *st) {
  uint32_t capacity = st->capacity;

  if (capacity == st->size) {
    if (capacity < LAVA_SYMVEC_MIN_GROW) {
      capacity = LAVA_SYMVEC_MIN_GROW;
    } else if (capacity >= LAVA_SYMVEC_MAX_GROW) {
      capacity += LAVA_SYMVEC_MAX_GROW;
    } else {
      capacity *= 2;
    }

    lava_symbol **symbols = realloc(st->symbols,
                                    capacity * sizeof(lava_symbol *));
    if (!symbols) {
      return lava_id_undefined;
    }
    st->symbols = symbols;
    st->capacity = capacity;
  }

  st->symbols[st->size] = NULL;
  return st->size++;
}

lava_symbol *lava_symtab_init_reserved(lava_symtab *st, lava_symbol *templ,
                                       uint32_t *ids) {
  // WRITEME
  lava_symbol *s;
  lava_attr_info *attrs;
  uint32_t size;

  // Make sure it's only reserved and not initialized.
  if (st->symbols[templ->id]) {
    return NULL;
  }

  size = (uint32_t)(sizeof(lava_symbol) + templ->count * sizeof(lava_attr_info));
  attrs = LAVA_CHECKED_ALLOCA(templ->count * sizeof(lava_attr_info));

  for (uint32_t i = 0; i < templ->count; ++i) {
    append_attr(&size, &attrs[i], st->symbols[ids[i]]);
  }

  if (!(s = malloc(size))) {
    return NULL;
  }

  *s = *templ;
  if (!lava_symtab_insert_name(st, s)) {
    free(s);
    return NULL;
  }
  memcpy(s->attrs, attrs, templ->count * sizeof(lava_attr_info));

  st->symbols[templ->id] = s;
  return s;
}

lava_symbol *lava_symbol_new(lava_symtab *st, uint32_t parent,
                             const char *name, uint32_t len,
                             uint32_t idcount, ...) {
  va_list va;
  uint32_t size;
  lava_attr_info *attrs;

  size = (uint32_t)(sizeof(lava_symbol) + idcount * sizeof(lava_attr_info));
  attrs = LAVA_CHECKED_ALLOCA(idcount * sizeof(lava_attr_info));

  va_start(va, idcount);
  for (uint32_t i = 0; i < idcount; ++i) {
    append_attr(&size, &attrs[i], st->symbols[va_arg(va, uint32_t)]);
  }
  va_end(va);

  return init_symbol(st, size, parent, name, len, idcount, attrs);
}

lava_symbol *lava_symbol_newv(lava_symtab *st, uint32_t parent,
                              const char *name, uint32_t len,
                              uint32_t idcount, uint32_t *ids) {
  uint32_t size;
  lava_attr_info *attrs;

  size = (uint32_t)(sizeof(lava_symbol) + idcount * sizeof(lava_attr_info));
  attrs = LAVA_CHECKED_ALLOCA(idcount * sizeof(lava_attr_info));

  for (uint32_t i = 0; i < idcount; ++i) {
    append_attr(&size, &attrs[i], st->symbols[ids[i]]);
  }

  return init_symbol(st, size, parent, name, len, idcount, attrs);
}

bool lava_symtab_insert_name(lava_symtab *st, lava_symbol *s) {
  const char *name = s->name;
  unsigned char *stname;
  uint32_t len = s->namelen;
  if (!name) {
    // If there is no name, call it "{id bytes}"
    // FIXME could overlap
    name = (char *)&s->id;
    len = sizeof(uint32_t);
  }
  stname = LAVA_CHECKED_ALLOCA(len + sizeof(uint32_t));
  memcpy(stname, &s->parent, sizeof(uint32_t));
  memcpy(stname + sizeof(uint32_t), name, len);

  if (!raxTryInsert(st->names, stname, len + sizeof(uint32_t), s, NULL)) {
    // Name already exists.
    return false;
  }
  return true;
}

lava_symbol *lava_symtab_find_name(lava_symtab *st, uint32_t parent,
                                   const char *name, uint32_t len) {
  // Ugh there has to be a better way to do this.
  unsigned char *stname = LAVA_CHECKED_ALLOCA(4 + len);
  memcpy(stname, &parent, 4);
  memcpy(stname + 4, name, len);
  void *s = raxFind(st->names, stname, len + 4);
  if (s == raxNotFound) {
    return NULL;
  }
  return s;
}
