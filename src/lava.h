#ifndef LAVA_H_
#define LAVA_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdalign.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef LAVA_ALLOCA_MAX
# define LAVA_ALLOCA_MAX (4*1024)
#endif

#define LAVA_CHECKED_ALLOCA(Size) \
  (assert(Size <= LAVA_ALLOCA_MAX), alloca(Size))

#define LAVA_STRUCT(Name) typedef struct lava_##Name lava_##Name; \
  struct lava_##Name

LAVA_STRUCT(str_i) {
  size_t      (*get_size)(const void *self);
  const char *(*get_data)(const void *self);
};

LAVA_STRUCT(str) {
  uint32_t size;
  char data[0];
};

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* LAVA_H_ */
