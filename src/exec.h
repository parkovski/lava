#ifndef LAVA_EXEC_H_
#define LAVA_EXEC_H_

#include "symtab.h"

LAVA_STRUCT(instr) {
  uint32_t op;
  union {
    uint32_t args[3];
    struct {
      uint32_t arg0;
      uint64_t argu;
    };
  };
};

LAVA_STRUCT(stack) {
  char *ptr;
  char *top;
  char *bot;
};

LAVA_STRUCT(exectx) {
  lava_instr *instr;
  const char *rodata;
  lava_stack  stack;
  lava_symtab symtab;
};

void lava_exec(lava_exectx *ctx);
void lava_step(lava_exectx *ctx);

#endif /* LAVA_EXEC_H_ */
