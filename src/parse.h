#ifndef LAVA_PARSE_H_
#define LAVA_PARSE_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

enum {
  lava_tk_eof  = -1,
  lava_tk_none = 0,

  lava_tk_newline,
  lava_tk_space,
  lava_tk_linecomment,

  lava_tk_text,
  lava_tk_string,
  lava_tk_int,
  lava_tk_float,

  lava_tk_comma,
  lava_tk_colon,
  lava_tk_lparen,
  lava_tk_rparen,
  lava_tk_lsquare,
  lava_tk_rsquare,
  lava_tk_lbrace,
  lava_tk_rbrace,
  lava_tk_equal,
};

extern const char *lava_tkname(int kind);

typedef struct {
  size_t   ofs;  // Byte offset in the source text.
  unsigned line; // 1-based line number.
  unsigned col;  // 1-based column number.
} lava_text_loc;

typedef struct {
  lava_text_loc start;
  lava_text_loc end;
} lava_text_span;

typedef struct {
  lava_text_span span;
  int            kind;
} lava_token;

enum {
  lava_diag_error = 0,
  lava_diag_warn = 1,
  lava_diag_char = 2,
};

typedef struct lava_parser_ctx lava_parser_ctx;
struct lava_parser_ctx {
  const char *buf; // Start of buffer.
  size_t      len; // Buffer length.

  lava_token    tok; // Current token.
  lava_text_loc loc; // Scanner location - end of next token.
  int           nxt; // Next token kind.

  unsigned tabstop; // Tabstop length.

  void (*diag)(lava_parser_ctx *ctx, unsigned flags, const char *msg);
};

typedef struct {
  lava_token op;
  lava_token arg[3];
} lava_instr_node;

/**
 * Initialize parser.
 * @param ctx Parser context to initialize.
 * @param src Source text.
 * @param len Source length, or 0 to calculate with `strlen`.
 */
void lava_parser_init(lava_parser_ctx *ctx, const char *src, size_t len);

void lava_parser_fini(lava_parser_ctx *ctx);

void lava_scan(lava_parser_ctx *ctx);
void lava_parse_instr(lava_parser_ctx *ctx, lava_instr_node *instr);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* LAVA_PARSE_H_ */
