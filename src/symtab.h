#ifndef LAVA_SYMTAB_H_
#define LAVA_SYMTAB_H_

#include "symbol.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef LAVA_SYMVEC_MIN_GROW
# define LAVA_SYMVEC_MIN_GROW 256
#endif

#ifndef LAVA_SYMVEC_MAX_GROW
# define LAVA_SYMVEC_MAX_GROW 4096
#endif

LAVA_STRUCT(symtab_i) {
  bool         (*init)(void *self);
  void         (*fini)(void *self);

  lava_symbol *(*get_symbol)(void *self, uint32_t id);
  lava_symbol *(*new_symbol)(void *self, uint32_t parent, const char *name,
                             uint32_t name_len, uint32_t id_count, ...);
  lava_symbol *(*new_symbol_v)(void *self, uint32_t parent, const char *name,
                               uint32_t name_len, uint32_t id_count,
                               uint32_t *ids);
  lava_symbol *(*find_name)(void *self, uint32_t parent, const char *name,
                            uint32_t name_len);
};

LAVA_STRUCT(symtab) {
  lava_symbol **symbols;
  uint32_t      capacity;
  uint32_t      size;
  rax          *names;
};

/// Returns false if memory allocation failed.
bool lava_symtab_init(lava_symtab *st);

/// Frees the symref array. Does not free the symbol data.
void lava_symtab_fini(lava_symtab *st);

/// Returns the index of the next available symbol pointer.
/// If memory is not available, returns `lava_id_undefined`.
uint32_t lava_symtab_reserve(lava_symtab *st);

lava_symbol *lava_symtab_init_reserved(lava_symtab *st, lava_symbol *templ,
                                       uint32_t *ids);

lava_symbol *lava_symbol_new(lava_symtab *st, uint32_t parent,
                             const char *name, uint32_t len,
                             uint32_t idcount, ...);

lava_symbol *lava_symbol_newv(lava_symtab *st, uint32_t parent,
                              const char *name, uint32_t len,
                              uint32_t idcount, uint32_t *ids);

bool lava_symtab_insert_name(lava_symtab *st, lava_symbol *s);

lava_symbol *lava_symtab_find_name(lava_symtab *st, uint32_t parent,
                                   const char *name, uint32_t len);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* LAVA_SYMTAB_H_ */
