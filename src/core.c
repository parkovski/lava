#include "symtab.h"

#include <stdlib.h>
#include <stdarg.h>
#include <alloca.h>

// Allocate symbols that don't exist by manually passing the relevant info.
// The va_list contains count * uint32_t[id, size, align].
static lava_symbol *alloc_symbol(lava_symbol *template, ...) {
  lava_symbol *s;
  va_list va;
  uint32_t size;
  uint32_t *ids;
  uint32_t *offsets;
  const uint32_t count = template->count;

  assert(count * 2 * sizeof(uint32_t) <= LAVA_ALLOCA_MAX);
  ids = alloca(count * sizeof(uint32_t));
  offsets = alloca(count * sizeof(uint32_t));

  size = sizeof(lava_symbol) + count * sizeof(lava_attr_info);

  va_start(va, template);
  for (uint32_t i = 0; i < count; ++i) {
    ids[i] = va_arg(va, uint32_t);
    uint32_t sz = va_arg(va, uint32_t);
    uint32_t al = va_arg(va, uint32_t);
    if (sz == 0) {
      offsets[i] = 0;
    } else {
      size = (size + al - 1) & -al;
      offsets[i] = size;
      size += sz;
    }
  }
  va_end(va);

  s = malloc(size);
  if (!s) {
    return NULL;
  }

  *s = (lava_symbol) {
    .id      = template->id,
    .parent  = template->parent,
    .name    = template->name,
    .namelen = template->namelen,
    .count   = template->count,
  };
  for (uint32_t i = 0; i < count; ++i) {
    s->attrs[i] = (lava_attr_info) {
      .id = ids[i],
      .offset = offsets[i],
    };
  }

  return s;
}

bool lava_symtab_add_core(lava_symtab *st) {
  lava_symbol template;
  lava_symbol *s;
  lava_symbol **syms = st->symbols;

  static_assert(_lava_core_id_count <= LAVA_SYMVEC_MIN_GROW,
                "Starting capacity too low");

#define SETATTR(N, Name) \
  (lava_##Name *)lava_symbol_attr(s, N) =

#define SETSTRUCT(N, Name) \
  *(lava_##Name *)lava_symbol_attr(s, N) = (lava_##Name)

#define ALLOC0(Name, Parent)                          \
  do {                                                \
    assert(st->size == lava_id_##Name);               \
    template = (lava_symbol) {                        \
      .id = lava_id_##Name,                           \
      .parent = lava_id_##Parent,                     \
      .name = #Name,                                  \
      .namelen = (sizeof #Name) - 1,                  \
      .count = 0,                                     \
    };                                                \
    if (!(s = alloc_symbol(&template))) return false; \
    if (!lava_symtab_insert_name(st, s)) return false;\
    ++st->size;                                       \
    syms[lava_id_##Name] = s;                         \
  } while (0)

#define ALLOC(Name, Parent, Count, ...)                            \
  do {                                                             \
    assert(st->size == lava_id_##Name);                            \
    template = (lava_symbol) {                                     \
      .id = lava_id_##Name,                                        \
      .parent = lava_id_##Parent,                                  \
      .name = #Name,                                               \
      .namelen = (sizeof #Name) - 1,                               \
      .count = Count,                                              \
    };                                                             \
    if (!(s = alloc_symbol(&template, __VA_ARGS__))) return false; \
    if (!lava_symtab_insert_name(st, s)) return false;             \
    ++st->size;                                                    \
    syms[lava_id_##Name] = s;                                      \
  } while (0)

#define ARGS(Name) lava_id_##Name, sizeof(lava_##Name), alignof(lava_##Name)

  ALLOC0(empty, undefined);

  ALLOC(mem_info32, empty, 1, ARGS(mem_info32));
  SETSTRUCT(0, mem_info32) {
    .size = 4,
    .align = LAVA_MEM_INFO_ALIGN_4B,
  };

  ALLOC(mem_info64, empty, 1, ARGS(mem_info32));
  SETSTRUCT(0, mem_info32) {
    .size = 8,
    .align = LAVA_MEM_INFO_ALIGN_8B,
  };

  ALLOC(attr_info, empty, 1, ARGS(mem_info32));
  SETSTRUCT(0, mem_info32) {
    .size = sizeof(lava_attr_info),
    .align = LAVA_MEM_INFO_ALIGN_4B,
  };

  ALLOC(bool, empty, 1, ARGS(mem_info32));
  SETSTRUCT(0, mem_info32) {
    .size = 1,
    .align = LAVA_MEM_INFO_ALIGN_1B,
  };

  ALLOC0(number, empty);

  ALLOC0(signmode, empty);

  ALLOC0(integer, empty);

  ALLOC0(float, empty);

  ALLOC0(decimal, empty);

#define ADD_INT_TYPE(Name, Bytes) \
  ALLOC(Name, empty, 1, ARGS(mem_info32)); \
  SETSTRUCT(0, mem_info32) { \
    .size = Bytes, \
    .align = LAVA_MEM_INFO_ALIGN_##Bytes##B, \
  }

#define ADD_FLT_TYPE(Name, Bytes) \
  ALLOC(Name, empty, 1, ARGS(mem_info32)); \
  SETSTRUCT(0, mem_info32) { \
    .size = Bytes, \
    .align = LAVA_MEM_INFO_ALIGN_##Bytes##B, \
  }

#define ADD_DEC_TYPE(Name, Bytes) \
  ALLOC(Name, empty, 1, ARGS(mem_info32)); \
  SETSTRUCT(0, mem_info32) { \
    .size = Bytes, \
    .align = LAVA_MEM_INFO_ALIGN_##Bytes##B, \
  }

  ADD_INT_TYPE(i8,   1);
  ADD_INT_TYPE(i16,  2);
  ADD_INT_TYPE(i32,  4);
  ADD_INT_TYPE(i64,  8);
  ADD_INT_TYPE(i128, 16);
  ADD_INT_TYPE(i256, 32);

  ADD_FLT_TYPE(f16,  2);
  ADD_FLT_TYPE(f32,  4);
  ADD_FLT_TYPE(f64,  8);
  ADD_FLT_TYPE(f128, 16);

  ADD_DEC_TYPE(d32,  4);
  ADD_DEC_TYPE(d64,  8);
  ADD_DEC_TYPE(d128, 16);

#undef ADD_INT_TYPE
#undef ADD_FLT_TYPE
#undef ADD_DEC_TYPE

#undef SETATTR
#undef SETSTRUCT
#undef ALLOC0
#undef ALLOC
#undef ARGS

  assert(st->size == _lava_core_id_count);
  return true;
}
