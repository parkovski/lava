#include "exec.h"
#include "op.h"

#include <assert.h>

const char *lava_op_name[_lava_op_count] = {
#define LAVA_OP_NAME(X) #X,
  LAVA_OPS(LAVA_OP_NAME)
#undef LAVA_OP_NAME
};

#define OPFN(Name) \
  static void do_##Name(lava_exectx *ctx, uint32_t args[3])

OPFN(nop) {
  (void)ctx;
  (void)args;
}

OPFN(abort) {
  (void)args;

  ctx->instr = NULL;
}

OPFN(debug) {
  assert(0);
}

OPFN(break) {
  assert(0);
}

OPFN(mov) {
}

OPFN(ldimm) {
}

OPFN(ldro) {
}

OPFN(ldind) {
}

OPFN(store) {
}

OPFN(not) {
}

OPFN(neg) {
}

OPFN(zext) {
}

OPFN(sext) {
}

OPFN(add) {
}

OPFN(sub) {
}

OPFN(mul) {
}

OPFN(fma) {
}

OPFN(div) {
}

OPFN(sdiv) {
}

OPFN(rem) {
}

OPFN(srem) {
}

OPFN(dvrm) {
}

OPFN(sdvrm) {
}

OPFN(and) {
}

OPFN(or) {
}

OPFN(xor) {
}

OPFN(shl) {
}

OPFN(shr) {
}

OPFN(sar) {
}

OPFN(eq) {
}

OPFN(ne) {
}

OPFN(lt) {
}

OPFN(le) {
}

OPFN(gt) {
}

OPFN(ge) {
}

OPFN(jmp) {
}

OPFN(cjmp) {
}

OPFN(ijmp) {
}

OPFN(cijmp) {
}

OPFN(call) {
}

OPFN(icall) {
}

void lava_exec(lava_exectx *ctx) {
  while (ctx->instr) {
    lava_step(ctx);
  }
}

void lava_step(lava_exectx *ctx) {
  lava_instr *instr = ctx->instr;
  ++ctx->instr;

  switch (instr->op) {
  default:
    break;

#define LAVA_OP_CASE(X) \
  case lava_op_##X: \
    do_##X(ctx, instr->args); \
    break;

  LAVA_OPS(LAVA_OP_CASE)
#undef LAVA_OP_CASE
  }
}
