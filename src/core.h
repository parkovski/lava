#ifndef LAVA_CORE_H_
#define LAVA_CORE_H_

#include "lava.h"

#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

struct lava_symtab;

/// Add core symbols to the symbol table.
bool lava_symtab_add_core(struct lava_symtab *st);

enum {
  lava_id_undefined = -1,
  lava_id_empty = 0,
  lava_id_mem_info32,
  lava_id_mem_info64,
  lava_id_attr_info,
  lava_id_bool,
  lava_id_number,
  lava_id_signmode,
  lava_id_integer,
  lava_id_float,
  lava_id_decimal,
  lava_id_i8,
  lava_id_i16,
  lava_id_i32,
  lava_id_i64,
  lava_id_i128,
  lava_id_i256,
  lava_id_f16,
  lava_id_f32,
  lava_id_f64,
  lava_id_f128,
  lava_id_d32,
  lava_id_d64,
  lava_id_d128,
  _lava_core_id_count,
};

#define LAVA_MEM_INFO_ALIGN_1B       0
#define LAVA_MEM_INFO_ALIGN_2B       1
#define LAVA_MEM_INFO_ALIGN_4B       2
#define LAVA_MEM_INFO_ALIGN_8B       3
#define LAVA_MEM_INFO_ALIGN_16B      4
#define LAVA_MEM_INFO_ALIGN_32B      5
#define LAVA_MEM_INFO_ALIGN_64B      6
#define LAVA_MEM_INFO_ALIGN_128B     7
#define LAVA_MEM_INFO64_ALIGN_256B   8
#define LAVA_MEM_INFO64_ALIGN_512B   9
#define LAVA_MEM_INFO64_ALIGN_1KiB   10
#define LAVA_MEM_INFO64_ALIGN_2KiB   11
#define LAVA_MEM_INFO64_ALIGN_4KiB   12
#define LAVA_MEM_INFO64_ALIGN_8KiB   13
#define LAVA_MEM_INFO64_ALIGN_16KiB  14
#define LAVA_MEM_INFO64_ALIGN_32KiB  15
#define LAVA_MEM_INFO64_ALIGN_64KiB  16
#define LAVA_MEM_INFO64_ALIGN_128KiB 17
#define LAVA_MEM_INFO64_ALIGN_256KiB 18
#define LAVA_MEM_INFO64_ALIGN_512KiB 19
#define LAVA_MEM_INFO64_ALIGN_1MiB   20
#define LAVA_MEM_INFO64_ALIGN_2MiB   21
#define LAVA_MEM_INFO64_ALIGN_4MiB   22
#define LAVA_MEM_INFO64_ALIGN_8MiB   23
#define LAVA_MEM_INFO64_ALIGN_16MiB  24
#define LAVA_MEM_INFO64_ALIGN_32MiB  25
#define LAVA_MEM_INFO64_ALIGN_64MiB  26
#define LAVA_MEM_INFO64_ALIGN_128MiB 27
#define LAVA_MEM_INFO64_ALIGN_256MiB 28
#define LAVA_MEM_INFO64_ALIGN_512MiB 29
#define LAVA_MEM_INFO64_ALIGN_1GiB   30
#define LAVA_MEM_INFO64_ALIGN_2GiB   31

#if SIZE_MAX == UINT32_MAX
# define LAVA_WORD_SIZE 32
# define LAVA_MEM_INFO_ALIGN_WORD 2
#elif SIZE_MAX == UINT64_MAX
# define LAVA_WORD_SIZE 64
# define LAVA_MEM_INFO_ALIGN_WORD 3
#else
# error "Can't get word size from SIZE_MAX"
#endif

#define LAVA_MEM_INFO32_MAX_ALIGN 7
#define LAVA_MEM_INFO64_MAX_ALIGN 31

#define LAVA_MEM_INFO32_MAX_SIZE UINT32_C(0x1FFFFFFF)
#define LAVA_MEM_INFO64_MAX_SIZE UINT64_C(0x07FFFFFFFFFFFFFF)

LAVA_STRUCT(meminfo_i) {
  size_t (*get_size)(const void *self);
  unsigned (*get_align)(const void *self);
};

// Defines the size and alignment of an instantiation of a symbol.
LAVA_STRUCT(mem_info32) {
  // Size in bytes (max 0x1FFF_FFFF => 512MiB - 1B).
  uint32_t size  : 29;
  // Alignment as a power of 2 (max 7 => 128B).
  uint32_t align : 3;
};

// Defines the size and alignment of an instantiation of a symbol.
LAVA_STRUCT(mem_info64) {
  // Size in bytes (max 0x07FF_FFFF_FFFF_FFFF => 512PiB - 1B).
  uint64_t size  : 59;
  // Alignment as a power of 2 (max 31 => 2GiB).
  uint64_t align : 5;
};

LAVA_STRUCT(attr_info_i) {
  uint32_t (*get_id)(const void *self);
  uint32_t (*get_offset)(const void *self);
};

// Attribute descriptor.
LAVA_STRUCT(attr_info) {
  // Attribute type symbol ID.
  uint32_t id;
  // Offset of the attribute data from the symbol's address.
  uint32_t offset;
};

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* LAVA_CORE_H_ */
