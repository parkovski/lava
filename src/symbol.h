#ifndef LAVA_SYMBOL_H_
#define LAVA_SYMBOL_H_

#include "core.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "rax/rax.h"

LAVA_STRUCT(symbol_i) {
  void      (*fini)           (void *self);

  uint32_t  (*get_id)         (const void *self);
  uint32_t  (*get_parent)     (const void *self);
  lava_str *(*get_name)       (const void *self);
  uint32_t  (*get_attr_count) (const void *self);
  void *    (*get_attr)       (const void *self, uint32_t index);
  uint32_t  (*find_attr_index)(const void *self, uint32_t id);
  void *    (*find_attr)      (const void *self, uint32_t id);
};

LAVA_STRUCT(symbol) {
  // Symbol table index.
  uint32_t       id;
  uint32_t       parent;
  const char    *name; /* TODO Intern */
  uint32_t       namelen;
  // Number of attributes.
  uint32_t       count;
  // Attribute descriptors.
  lava_attr_info attrs[0];
};

/// @param n Attribute index.
#define lava_symbol_attr(s, n) ((void *)((char *)(s) + (s)->attrs[n].offset))

/// @param id Attribute symbol ID.
uint32_t lava_symbol_find_attr_index(lava_symbol *s, uint32_t id);

/// @param id Attribute symbol ID.
void *lava_symbol_find_attr(lava_symbol *s, uint32_t id);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* LAVA_SYMBOL_H_ */
