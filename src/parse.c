#include "parse.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define DEFAULT_BUF_SIZE 1024

static const char *const tknames[] = {
  "eof",
  "none",
  "newline",
  "space",
  "linecomment",
  "text",
  "string",
  "int",
  "float",
  "comma",
  "colon",
  "lparen",
  "rparen",
  "lsquare",
  "rsquare",
  "lbrace",
  "rbrace",
  "equal",
};

const char *lava_tkname(int kind) {
  // move from -1 offset (eof) to 0 offset.
  ++kind;
  if (kind >= sizeof(tknames) / sizeof(tknames[0])) {
    return "<unknown>";
  }
  return tknames[kind];
}

static void diag(lava_parser_ctx *ctx, unsigned flags, const char *msg) {
  if (flags & lava_diag_char) {
    printf("%d:%d: %s: %s (found ", ctx->loc.line, ctx->loc.col,
           (flags & lava_diag_warn) ? "warning" : "error", msg);
    int c = ctx->buf[ctx->loc.ofs];
    if (isprint(c)) {
      printf("'%c')\n", c);
    } else {
      printf("'\\x%02X')\n", c);
    }
    return;
  }

  printf("%d:%d: %s: %s (found ",
         ctx->tok.span.start.line, ctx->tok.span.start.col,
         (flags & lava_diag_warn) ? "warning" : "error", msg);
  if (ctx->tok.kind >= lava_tk_text && ctx->tok.kind <= lava_tk_float) {
    printf("%.*s)\n", (int)(ctx->tok.span.end.ofs - ctx->tok.span.start.ofs),
           ctx->buf + ctx->tok.span.start.ofs);
  } else {
    printf("%s)\n", tknames[ctx->tok.kind + 1]);
  }
}

void lava_parser_init(lava_parser_ctx *ctx, const char *src, size_t len) {
  ctx->buf = src;
  ctx->len = len ? len : strlen(src);

  ctx->loc = ctx->tok.span.start = ctx->tok.span.end =
    (lava_text_loc) {
      .ofs  = 0,
      .line = 1,
      .col  = 1,
    };
  ctx->nxt = ctx->tok.kind = lava_tk_none;

  ctx->tabstop = 4;

  ctx->diag = diag;

  // Get lookahead token.
  lava_scan(ctx);
  // Get current token.
  lava_scan(ctx);
}

void lava_parser_fini(lava_parser_ctx *ctx) {
}

static void advance_token(lava_parser_ctx *ctx) {
  ctx->tok.span.start = ctx->tok.span.end;
  ctx->tok.span.end = ctx->loc;
  ctx->tok.kind = ctx->nxt;
}

static int next_char(lava_parser_ctx *ctx) {
  if (ctx->loc.ofs == ctx->len) {
    return -1;
  }
  char c = ctx->buf[ctx->loc.ofs];
  if (c == '\n') {
    ++ctx->loc.line;
    ctx->loc.col = 1;
  } else if (c == '\t') {
    unsigned n = ctx->loc.col + ctx->tabstop - 1;
    ctx->loc.col = n - n % ctx->tabstop;
  } else {
    ++ctx->loc.col;
  }
  return ctx->buf[++ctx->loc.ofs];
}

static void read_space(lava_parser_ctx *ctx) {
  int c;
  ctx->nxt = lava_tk_space;
  while ((c = next_char(ctx)), c == ' ' || c == '\t' || c == '\r');
}

static void read_line_comment(lava_parser_ctx *ctx) {
  int c;
  ctx->nxt = lava_tk_linecomment;
  while ((c = next_char(ctx)) != -1 && c != '\n');
}

static void read_text(lava_parser_ctx *ctx) {
  int c;
  ctx->nxt = lava_tk_text;
  while ((c = next_char(ctx))) {
    int lower = c | 0x20;
    if (!(
      (lower >= 'a' && lower <= 'z') ||
      (c >= '0' && c <= '9') ||
      c == '_'
    )) {
      break;
    }
  }
}

static void read_string(lava_parser_ctx *ctx, char quote) {
  int c;
  ctx->nxt = lava_tk_string;
  while (true) {
    if ((c = next_char(ctx)) == -1) {
      ctx->diag(ctx, lava_diag_char, "unclosed string");
      return;
    }
    if (c == quote) {
      // Include last quote.
      next_char(ctx);
      break;
    }
    if (c == '\\') {
      if ((c = next_char(ctx)) == -1) {
        ctx->diag(ctx, lava_diag_char, "expected escape after '\\'");
        return;
      }
      // TODO read escapes
    }
  }
}

static void read_number(lava_parser_ctx *ctx) {
  int c = ctx->buf[ctx->loc.ofs];
  if (c != '.') {
    while ((c = next_char(ctx)), c >= '0' && c <= '9');
    if (c != '.') {
      ctx->nxt = lava_tk_int;
      return;
    }
  }

  while ((c = next_char(ctx)), c >= '0' && c <= '9');
  ctx->nxt = lava_tk_float;
}

void lava_scan(lava_parser_ctx *ctx) {
  advance_token(ctx);

  if (ctx->loc.ofs == ctx->len) {
    ctx->nxt = lava_tk_eof;
    return;
  }

  const char c = ctx->buf[ctx->loc.ofs];
  switch (c) {
  case ' ':
  case '\t':
  case '\r':
    read_space(ctx);
    break;

  case '#':
    read_line_comment(ctx);
    break;

  case '"':
  case '\'':
    read_string(ctx, c);
    break;

#define SINGLE(Ch, Kind)       \
  case Ch:                     \
    ctx->nxt = lava_tk_##Kind; \
    next_char(ctx);            \
    break

  SINGLE('\n', newline);
  SINGLE(',', comma);
  SINGLE(':', colon);
  SINGLE('(', lparen);
  SINGLE(')', rparen);
  SINGLE('[', lsquare);
  SINGLE(']', rsquare);
  SINGLE('{', lbrace);
  SINGLE('}', rbrace);

#undef SINGLE

  default: {
    const char lower = c | 0x20;
    if ((lower >= 'a' && lower <= 'z') || c == '_') {
      read_text(ctx);
    } else if ((c >= '0' && c <= '9') || c == '.') {
      read_number(ctx);
    } else {
      ctx->nxt = lava_tk_none;
      ctx->diag(ctx, lava_diag_char, "invalid character");
      next_char(ctx);
    }
  }
    break;
  }
}

static void next_token(lava_parser_ctx *ctx, bool skipnl) {
  int kind;
  do {
    lava_scan(ctx);
    kind = ctx->tok.kind;
  } while (kind == lava_tk_space || kind == lava_tk_linecomment
        || (kind == lava_tk_newline && skipnl));
}

static void parse_args(lava_parser_ctx *ctx, lava_instr_node *instr) {
  unsigned n = 0;
  do {
    switch (ctx->tok.kind) {
    case lava_tk_text:
    case lava_tk_string:
    case lava_tk_int:
    case lava_tk_float:
      instr->arg[n] = ctx->tok;
      break;

    default:
      return;
    }

    next_token(ctx, false);

    if (ctx->tok.kind != lava_tk_comma) {
      return;
    }
    if (++n > 2) {
      ctx->diag(ctx, 0, "too many args");
      return;
    }
    next_token(ctx, true);
  } while (true);
}

void lava_parse_instr(lava_parser_ctx *ctx, lava_instr_node *instr) {
  next_token(ctx, true);
  if (ctx->tok.kind == lava_tk_eof) {
    return;
  }
  if (ctx->tok.kind != lava_tk_text) {
    ctx->diag(ctx, 0, "expected instruction");
    return;
  }
  instr->op = ctx->tok;
  next_token(ctx, false);
  parse_args(ctx, instr);
  if (ctx->tok.kind != lava_tk_newline && ctx->tok.kind != lava_tk_eof) {
    ctx->diag(ctx, 0, "expected newline");
  }
}
