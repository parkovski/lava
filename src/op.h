#ifndef LAVA_OP_H_
#define LAVA_OP_H_

#define LAVA_OPS(OP) \
  OP(nop) \
  \
  OP(abort) \
  OP(debug) \
  OP(break) \
  \
  OP(mov) \
  OP(ldimm) \
  OP(ldro) \
  OP(ldind) \
  OP(store) \
  \
  OP(not) \
  OP(neg) \
  \
  OP(zext) \
  OP(sext) \
  \
  OP(add) \
  OP(sub) \
  OP(mul) \
  OP(fma) \
  OP(div) \
  OP(sdiv) \
  OP(rem) \
  OP(srem) \
  OP(dvrm) \
  OP(sdvrm) \
  \
  OP(and) \
  OP(or) \
  OP(xor) \
  \
  OP(shl) \
  OP(shr) \
  OP(sar) \
  \
  OP(eq) \
  OP(ne) \
  OP(lt) \
  OP(le) \
  OP(gt) \
  OP(ge) \
  \
  OP(jmp) \
  OP(cjmp) \
  OP(ijmp) \
  OP(cijmp) \
  OP(call) \
  OP(icall) \

enum {
#define LAVA_OP_ID(X) lava_op_##X,
  LAVA_OPS(LAVA_OP_ID)
#undef LAVA_OP_ID
  _lava_op_count,
};

extern const char *lava_op_name[_lava_op_count];

#endif /* LAVA_OP_H_ */
